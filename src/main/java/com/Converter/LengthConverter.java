package com.Converter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class LengthConverter extends JPanel {
    enum Convert {
        m("meter                              "),
        km("kilometer"),
        mile ("mile"),
        n_mile("nautical mile"),
        cable("cable"),
        league("league"),
        foot("foot"),
        yard("yard");


        private String description;

        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final Convert from;
        private final Convert to;

        public ConvertPair(Convert from, Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ConvertPair that = (ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }

    private final Map<ConvertPair, BigDecimal> exchangeRates = new HashMap<ConvertPair, BigDecimal>() {
        {
            put(new ConvertPair(Convert.m, Convert.m), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.km, Convert.km), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.mile, Convert.mile), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.n_mile, Convert.n_mile), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.cable, Convert.cable), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.league, Convert.league), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.foot, Convert.foot), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.yard, Convert.yard), BigDecimal.valueOf(1));

            put(new ConvertPair(Convert.m, Convert.km), BigDecimal.valueOf(0.001));
            put(new ConvertPair(Convert.m, Convert.mile), BigDecimal.valueOf(0.000621371));
            put(new ConvertPair(Convert.m, Convert.n_mile), BigDecimal.valueOf(0.000539957));
            put(new ConvertPair(Convert.m, Convert.cable), BigDecimal.valueOf(0.0054));
            put(new ConvertPair(Convert.m, Convert.league), BigDecimal.valueOf(0.000179986));
            put(new ConvertPair(Convert.m, Convert.foot), BigDecimal.valueOf(3.28084));
            put(new ConvertPair(Convert.m, Convert.yard), BigDecimal.valueOf(1.09361));

            put(new ConvertPair(Convert.km, Convert.m), BigDecimal.valueOf(1000.0));
            put(new ConvertPair(Convert.km, Convert.mile), BigDecimal.valueOf(0.621371));
            put(new ConvertPair(Convert.km, Convert.n_mile), BigDecimal.valueOf(0.539957));
            put(new ConvertPair(Convert.km, Convert.cable), BigDecimal.valueOf(5.3961));
            put(new ConvertPair(Convert.km, Convert.league), BigDecimal.valueOf(0.179986));
            put(new ConvertPair(Convert.km, Convert.foot), BigDecimal.valueOf(3280.84));
            put(new ConvertPair(Convert.km, Convert.yard), BigDecimal.valueOf(1093.61));

            put(new ConvertPair(Convert.mile, Convert.m), BigDecimal.valueOf(1609.34));
            put(new ConvertPair(Convert.mile, Convert.km), BigDecimal.valueOf(1.60934));
            put(new ConvertPair(Convert.mile, Convert.n_mile), BigDecimal.valueOf(0.868976));
            put(new ConvertPair(Convert.mile, Convert.cable), BigDecimal.valueOf(8.6842));
            put(new ConvertPair(Convert.mile, Convert.league), BigDecimal.valueOf(0.289659));
            put(new ConvertPair(Convert.mile, Convert.foot), BigDecimal.valueOf(5280));
            put(new ConvertPair(Convert.mile, Convert.yard), BigDecimal.valueOf(1760));

            put(new ConvertPair(Convert.n_mile, Convert.m), BigDecimal.valueOf(1852));
            put(new ConvertPair(Convert.n_mile, Convert.km), BigDecimal.valueOf(1.852));
            put(new ConvertPair(Convert.n_mile, Convert.mile), BigDecimal.valueOf(1.15078));
            put(new ConvertPair(Convert.n_mile, Convert.cable), BigDecimal.valueOf(9.99));
            put(new ConvertPair(Convert.n_mile, Convert.league), BigDecimal.valueOf(0.333333));
            put(new ConvertPair(Convert.n_mile, Convert.foot), BigDecimal.valueOf(6076.12));
            put(new ConvertPair(Convert.n_mile, Convert.yard), BigDecimal.valueOf(2025.37));

            put(new ConvertPair(Convert.cable, Convert.m), BigDecimal.valueOf(185.2));
            put(new ConvertPair(Convert.cable, Convert.km), BigDecimal.valueOf(0.1853));
            put(new ConvertPair(Convert.cable, Convert.mile), BigDecimal.valueOf(0.115151515));
            put(new ConvertPair(Convert.cable, Convert.n_mile), BigDecimal.valueOf(0.1001));
            put(new ConvertPair(Convert.cable, Convert.league), BigDecimal.valueOf(0.038384092));
            put(new ConvertPair(Convert.cable, Convert.foot), BigDecimal.valueOf(608));
            put(new ConvertPair(Convert.cable, Convert.yard), BigDecimal.valueOf(202.66));

            put(new ConvertPair(Convert.league, Convert.m), BigDecimal.valueOf(5556));
            put(new ConvertPair(Convert.league, Convert.km), BigDecimal.valueOf(5.556));
            put(new ConvertPair(Convert.league, Convert.mile), BigDecimal.valueOf(3.45234));
            put(new ConvertPair(Convert.league, Convert.n_mile), BigDecimal.valueOf(3.0));
            put(new ConvertPair(Convert.league, Convert.cable), BigDecimal.valueOf(29.980));
            put(new ConvertPair(Convert.league, Convert.foot), BigDecimal.valueOf(18228.3));
            put(new ConvertPair(Convert.league, Convert.yard), BigDecimal.valueOf(6076.12));

            put(new ConvertPair(Convert.foot, Convert.m), BigDecimal.valueOf(0.3048));
            put(new ConvertPair(Convert.foot, Convert.km), BigDecimal.valueOf(0.0003048));
            put(new ConvertPair(Convert.foot, Convert.mile), BigDecimal.valueOf(0.000189394));
            put(new ConvertPair(Convert.foot, Convert.n_mile), BigDecimal.valueOf(0.000164579));
            put(new ConvertPair(Convert.foot, Convert.cable), BigDecimal.valueOf(0.0016));
            put(new ConvertPair(Convert.foot, Convert.league), BigDecimal.valueOf(6.31313E-5 ));
            put(new ConvertPair(Convert.foot, Convert.yard), BigDecimal.valueOf(0.333333));

            put(new ConvertPair(Convert.yard, Convert.m), BigDecimal.valueOf(0.9144));
            put(new ConvertPair(Convert.yard, Convert.km), BigDecimal.valueOf(0.0009144));
            put(new ConvertPair(Convert.yard, Convert.mile), BigDecimal.valueOf(0.000568182));
            put(new ConvertPair(Convert.yard, Convert.n_mile), BigDecimal.valueOf(0.000493737));
            put(new ConvertPair(Convert.yard, Convert.cable), BigDecimal.valueOf(0.004934210));
            put(new ConvertPair(Convert.yard, Convert.league), BigDecimal.valueOf(0.000164579));
            put(new ConvertPair(Convert.yard, Convert.foot), BigDecimal.valueOf(3.0));

        }
    };

    public LengthConverter() {
        super(new FlowLayout(FlowLayout.LEADING));


        // From
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);

        // Amount
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);


        // To
        JComboBox toOptions = new JComboBox(Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);

        // Convert Action
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }

    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {

        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }

                // Convert
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }

            private BigDecimal convertConvert(String amountInputText) {
                ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };

    }
}