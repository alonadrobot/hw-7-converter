package com.Converter;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(new ApplicationConverter.WeightConverter());
        frame.getContentPane().add(new TimeConverter());
        frame.getContentPane().add(new LengthConverter());
        frame.getContentPane().add(new VolumeConverter());
        frame.getContentPane().add(new Temperatures());
        frame.setTitle("Convert");
        frame.setSize(1200, 500);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
