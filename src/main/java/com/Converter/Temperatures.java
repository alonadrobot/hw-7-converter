package com.Converter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


class Temperatures extends JPanel {
    enum Convert {
        C("Celsius                             "),
        K("Kelvin scale"),
        F("Fahrenheit"),
        RE("Reaumur scale"),
        RO("Measurement scale"),
        RA("Rankine scale"),
        N("Newtonian scale"),
        D("Delisle scale");
        private String description;
        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final Convert from;
        private final Convert to;
        public ConvertPair(Convert from, Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ConvertPair that = (ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result =31* result + to.hashCode();
            return result;
        }
    }
    private final Map<ConvertPair, BigDecimal> exchangeRates = new HashMap<ConvertPair, BigDecimal>() {
        {
            put(new ConvertPair(Convert.C, Convert.C), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.F, Convert.F), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.K), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.RE, Convert.RE), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.RO, Convert.RO), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.RA, Convert.RA), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.N, Convert.N), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.D, Convert.D), BigDecimal.valueOf(1));

            put(new ConvertPair(Convert.K, Convert.C), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.F), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.RE), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.RO), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.RA), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.N), BigDecimal.valueOf(1));
            put(new ConvertPair(Convert.K, Convert.D), BigDecimal.valueOf(1));

            put(new ConvertPair(Convert.C, Convert.F), BigDecimal.valueOf(0.001));
            put(new ConvertPair(Convert.C, Convert.K), BigDecimal.valueOf(273.15));
            put(new ConvertPair(Convert.C, Convert.RE), BigDecimal.valueOf(0.219969248));
            put(new ConvertPair(Convert.C, Convert.RO), BigDecimal.valueOf(1.759753986));
            put(new ConvertPair(Convert.C, Convert.RA), BigDecimal.valueOf(0.879876993));
            put(new ConvertPair(Convert.C, Convert.N), BigDecimal.valueOf(0.006110256));
            put(new ConvertPair(Convert.C, Convert.D), BigDecimal.valueOf(0.03531466));

            put(new ConvertPair(Convert.F, Convert.C), BigDecimal.valueOf(273.15));
            put(new ConvertPair(Convert.F, Convert.K), BigDecimal.valueOf(273.15));
            put(new ConvertPair(Convert.F, Convert.RE), BigDecimal.valueOf(219.9692482991));
            put(new ConvertPair(Convert.F, Convert.RO), BigDecimal.valueOf(1759.753986393));
            put(new ConvertPair(Convert.F, Convert.RA), BigDecimal.valueOf(879.8769931964));
            put(new ConvertPair(Convert.F, Convert.N), BigDecimal.valueOf(6.110256897197));
            put(new ConvertPair(Convert.F, Convert.D), BigDecimal.valueOf(35.31466672149));

            put(new ConvertPair(Convert.RE, Convert.C), BigDecimal.valueOf(4.54609));
//            put(new ConvertPair(Convert.RE, Convert.K), BigDecimal.valueOf());
            put(new ConvertPair(Convert.RE, Convert.F), BigDecimal.valueOf(0.00454609));
            put(new ConvertPair(Convert.RE, Convert.RO), BigDecimal.valueOf(8.0));
            put(new ConvertPair(Convert.RE, Convert.RA), BigDecimal.valueOf(4.0));
            put(new ConvertPair(Convert.RE, Convert.N), BigDecimal.valueOf(0.027777777));
            put(new ConvertPair(Convert.RE, Convert.D), BigDecimal.valueOf(0.160543653));

            put(new ConvertPair(Convert.RO, Convert.C), BigDecimal.valueOf(0.56826125));
//            put(new ConvertPair(Convert.RO, Convert.C), BigDecimal.valueOf());
            put(new ConvertPair(Convert.RO, Convert.F), BigDecimal.valueOf(0.00056826125));
            put(new ConvertPair(Convert.RO, Convert.RE), BigDecimal.valueOf(0.125));
            put(new ConvertPair(Convert.RO, Convert.RA), BigDecimal.valueOf(0.5));
            put(new ConvertPair(Convert.RO, Convert.N), BigDecimal.valueOf(0.0034722));
            put(new ConvertPair(Convert.RO, Convert.D), BigDecimal.valueOf(0.0200679));

            put(new ConvertPair(Convert.RA, Convert.C), BigDecimal.valueOf(1.1365225));
//            put(new ConvertPair(Convert.RA, Convert.K), BigDecimal.valueOf());
            put(new ConvertPair(Convert.RA, Convert.F), BigDecimal.valueOf(0.0011365225));
            put(new ConvertPair(Convert.RA, Convert.RE), BigDecimal.valueOf(0.25));
            put(new ConvertPair(Convert.RA, Convert.RO), BigDecimal.valueOf(2.0));
            put(new ConvertPair(Convert.RA, Convert.N), BigDecimal.valueOf(0.006944444));
            put(new ConvertPair(Convert.RA, Convert.D), BigDecimal.valueOf(0.040135913));

            put(new ConvertPair(Convert.N, Convert.C), BigDecimal.valueOf(163.65924));
//            put(new ConvertPair(Convert.N, Convert.K), BigDecimal.valueOf());
            put(new ConvertPair(Convert.N, Convert.F), BigDecimal.valueOf(0.16365924));
            put(new ConvertPair(Convert.N, Convert.RE), BigDecimal.valueOf(36.0));
            put(new ConvertPair(Convert.N, Convert.RO), BigDecimal.valueOf(288.0));
            put(new ConvertPair(Convert.N, Convert.RA), BigDecimal.valueOf(144.0));
            put(new ConvertPair(Convert.N, Convert.D), BigDecimal.valueOf(5.779571516));

            put(new ConvertPair(Convert.D, Convert.C), BigDecimal.valueOf(28.3168465));
//            put(new ConvertPair(Convert.D, Convert.K), BigDecimal.valueOf());
            put(new ConvertPair(Convert.D, Convert.F), BigDecimal.valueOf(0.028316846));
            put(new ConvertPair(Convert.D, Convert.RE), BigDecimal.valueOf(6.2288354));
            put(new ConvertPair(Convert.D, Convert.RO), BigDecimal.valueOf(49.8306836));
            put(new ConvertPair(Convert.D, Convert.RA), BigDecimal.valueOf(24.9153418));
            put(new ConvertPair(Convert.D, Convert.N), BigDecimal.valueOf(0.17302320));
        }
    };
    public Temperatures() {
        super(new FlowLayout(FlowLayout.LEADING));
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);
        JComboBox toOptions = new JComboBox(Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }
    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }
            private BigDecimal convertConvert(String amountInputText) {
                ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };
    }
}
